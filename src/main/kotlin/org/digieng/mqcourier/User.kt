package org.digieng.mqcourier

import java.io.Serializable

data class User(
    val username: String,
    val password: String,
    val firstName: String = "",
    val lastName: String = ""
) : Serializable
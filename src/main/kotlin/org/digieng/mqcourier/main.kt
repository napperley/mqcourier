package org.digieng.mqcourier

import org.apache.log4j.Level
import org.apache.log4j.LogManager
import org.apache.log4j.Logger
import org.digieng.mqcourier.database.Database
import org.digieng.mqcourier.settings.MqttBrokerSettings
import org.digieng.mqcourier.settings.dbName
import org.slf4j.LoggerFactory
import kotlin.system.exitProcess

private val logger by lazy { LoggerFactory.getLogger("main") }

fun main(args: Array<String>) {
    logger.info("Starting MQTT Broker...")
    try {
        setupLogging()
        Database.connect("${System.getProperty("user.home")}/.mqcourier/database/$dbName")
        if ("--setupDb" in args) setupDb()
        MqttBroker(
            host = MqttBrokerSettings.host,
            port = MqttBrokerSettings.port,
            sslEnabled = MqttBrokerSettings.sslEnabled,
            authenticationEnabled = MqttBrokerSettings.authenticationEnabled,
            errorHandler = ::handleMqttBrokerError
        )
    } catch (ex: Exception) {
        handleException(ex)
    }
}

private fun handleException(ex: Exception) {
    logger.error("Serious problem encountered: ${ex.printStackTrace()}")
    Database.disconnect()
    exitProcess(-1)
}

private fun handleMqttBrokerError(msg: String, ex: Exception) {
    logger.error("MQTT Broker - $msg: ${ex.printStackTrace()}")
    Database.disconnect()
    exitProcess(-1)
}

private fun setupLogging() {
    @Suppress("UNCHECKED_CAST")
    val loggerList = (LogManager.getCurrentLoggers().toList() as List<Logger>)
    loggerList.forEach { if (it.name != logger.name) it.level = Level.OFF }
}
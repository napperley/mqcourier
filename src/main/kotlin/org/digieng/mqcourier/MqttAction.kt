package org.digieng.mqcourier

import java.io.Serializable

enum class MqttAction : Serializable {
    PUBLISH, SUBSCRIBE
}

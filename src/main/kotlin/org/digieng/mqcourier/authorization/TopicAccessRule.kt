package org.digieng.mqcourier.authorization

import org.digieng.mqcourier.MqttAction
import java.io.Serializable

internal data class TopicAccessRule(
    override val id: Int,
    override var mqttAction: MqttAction,
    val topic: String
) : AccessRule, Serializable {
    override val allowedGroups: MutableList<String> = mutableListOf()
    override val allowedUsers: MutableList<String> = mutableListOf()

    override fun accessAllowed(username: String): Boolean {
        var result = false
        if (username in allowedUsers) result = true
        return result
    }
}
package org.digieng.mqcourier.authorization

import org.digieng.mqcourier.MqttAction
import java.io.Serializable

interface AccessRule : Serializable {
    val id: Int
    val allowedGroups: MutableList<String>
    val allowedUsers: MutableList<String>
    var mqttAction: MqttAction

    fun accessAllowed(username: String): Boolean
}
package org.digieng.mqcourier

internal enum class SslType {
    JKS, PEM
}
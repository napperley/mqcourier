package org.digieng.mqcourier

import io.netty.handler.codec.mqtt.MqttConnectReturnCode
import io.netty.handler.codec.mqtt.MqttQoS
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.file.FileSystemException
import io.vertx.core.net.JksOptions
import io.vertx.core.net.PemKeyCertOptions
import io.vertx.mqtt.MqttAuth
import io.vertx.mqtt.MqttEndpoint
import io.vertx.mqtt.MqttServer
import io.vertx.mqtt.MqttServerOptions
import io.vertx.mqtt.messages.MqttPublishMessage
import io.vertx.mqtt.messages.MqttSubscribeMessage
import io.vertx.mqtt.messages.MqttUnsubscribeMessage
import org.digieng.mqcourier.authorization.TopicAccessRule
import org.digieng.mqcourier.database.AccessRuleRepo
import org.digieng.mqcourier.database.Database
import org.digieng.mqcourier.database.UserRepo
import org.digieng.mqcourier.settings.MqttBrokerSettings
import org.slf4j.LoggerFactory
import java.nio.charset.Charset

/**
 * Acts as a MQTT Broker which is compliant with the MQTT 3.1 standard.
 * @author Nick Apperley
 */
internal class MqttBroker(
    /** The FQDN or IPv4 address. */
    val host: String = "localhost",
    /** A whole number greater than 1024. */
    val port: Int = 1883,
    /** If true then all MQTT traffic is encrypted via SSL. */
    val sslEnabled: Boolean = false,
    @Suppress("MemberVisibilityCanBePrivate") val sslType: SslType = SslType.JKS,
    /** If true then authentication is applied to the MQTT Broker (all connecting clients will have to login). */
    val authenticationEnabled: Boolean = false,
    /** A function that will handle a MQTT Broker error. */
    val errorHandler: (String, Exception) -> Unit = { _, _ -> }
) {
    private val logger by lazy { LoggerFactory.getLogger("MqttBroker") }
    private val vertx = Vertx.vertx()
    private val mqttServer = MqttServer.create(vertx, createServerOptions(port))
    /** Holds functions which handle a client publish event. */
    val clientPublishHandlers = mutableListOf<(MqttEndpoint, MqttPublishMessage) -> Unit>()

    init {
        try {
            mqttServer.endpointHandler(::handleEndpoint).listen(port, host, ::handleListen)
        } catch (ex: Exception) {
            errorHandler("Failed to start MQTT Broker", ex)
        }
    }

    private fun createServerOptions(newPort: Int) = MqttServerOptions().apply {
        port = newPort
        if (sslEnabled) {
            when (sslType) {
                SslType.JKS -> keyStoreOptions = createJksOptions()
                SslType.PEM -> keyCertOptions = createPemKeyCertOptions()
            }
            isSsl = sslEnabled
        }
    }

    private fun createPemKeyCertOptions() = PemKeyCertOptions().apply {
        val basePath = "./src/main/resources/ssl"
        try {
            keyPath = "$basePath/server_key.pem"
            certPath = "$basePath/server_cert.pem"
        } catch (ex: Exception) {
            errorHandler("Problems encountered with setting up PEM key/certificate", ex)
        }
    }

    private fun createJksOptions() = JksOptions().apply {
        var buffer: Buffer? = null
        try {
            buffer = vertx.fileSystem().readFileBlocking("ssl/broker_keystore_password.txt")
            path = "/ssl/broker_keystore"
        } catch (fileSysEx: FileSystemException) {
            errorHandler("Can't read broker_keystore_password.txt, does the file exist?", fileSysEx)
        } catch (ex: Exception) {
            errorHandler("Problems encountered with setting up JKS", ex)
        }
        password = buffer?.toString(Charset.forName("UTF-8"))?.replace(oldValue = "\n", newValue = "")
    }

    private fun handleListen(ar: AsyncResult<MqttServer>) {
        if (ar.succeeded() && sslEnabled) logger.info("MQTT Broker (SSL enabled) listening on $host:$port")
        else if (ar.succeeded() && !sslEnabled) logger.info("MQTT Broker (SSL disabled) listening on $host:$port")
        else logger.error("Couldn't start the MQTT Broker: ${ar.cause().printStackTrace()}")
    }

    private fun handleEndpoint(endpoint: MqttEndpoint) {
        setupEndpointHandlers(endpoint)
        if (MqttBrokerSettings.logClientConnect) {
            logger.debug("MQTT client ${endpoint.clientIdentifier()} requesting connection (" +
                "${if (endpoint.isCleanSession) "clean" else "unclean"} session)")
        }
        if (authenticationEnabled) {
            if (authenticateUser(endpoint.auth())) endpoint.accept(false)
            else endpoint.reject(MqttConnectReturnCode.CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD)
        } else {
            endpoint.accept(false)
        }
    }

    private fun setupEndpointHandlers(endpoint: MqttEndpoint) {
        endpoint.disconnectHandler { handleDisconnect(endpoint) }
        endpoint.publishHandler { msg -> handlePublish(endpoint, msg) }
        endpoint.publishReleaseHandler { msgId -> handlePublishRelease(endpoint, msgId) }
        endpoint.subscribeHandler { subscribeMsg -> handleSubscribe(endpoint, subscribeMsg) }
        endpoint.unsubscribeHandler { unsubscribeMsg -> handleUnsubscribe(endpoint, unsubscribeMsg) }
    }

    private fun authenticateUser(auth: MqttAuth?): Boolean {
        var result = false

        if (auth != null) {
            val user = UserRepo.selectByUsername(auth.userName())
            if (user != null && user.password == auth.password()) result = true
        }
        return result
    }

    private fun handleDisconnect(endpoint: MqttEndpoint) {
        if (MqttBrokerSettings.logClientDisconnect) {
            logger.debug("Client ${endpoint.clientIdentifier()} disconnected from MQTT Broker")
        }
    }

    private fun handlePublishRelease(endpoint: MqttEndpoint, msgId: Int) {
        endpoint.publishComplete(msgId)
    }

    private fun handlePublish(endpoint: MqttEndpoint, msg: MqttPublishMessage) {
        val auth = endpoint.auth()
        if (auth != null &&
            !topicAccessAllowed(username = auth.userName(), topic = msg.topicName(), mqttAction = MqttAction.PUBLISH)) {
            endpoint.reject(MqttConnectReturnCode.CONNECTION_REFUSED_NOT_AUTHORIZED)
        } else {
            if (msg.qosLevel() == MqttQoS.AT_LEAST_ONCE) endpoint.publishAcknowledge(msg.messageId())
            else if (msg.qosLevel() == MqttQoS.EXACTLY_ONCE) endpoint.publishReceived(msg.messageId())
            clientPublishHandlers.forEach { it(endpoint, msg) }
        }
        if (MqttBrokerSettings.logClientPublish) {
            logger.debug("Incoming message on topic ${msg.topicName()}: ${msg.payload()} (QoS - ${msg.qosLevel()})")
        }
    }

    private fun handleSubscribe(endpoint: MqttEndpoint, subscribeMsg: MqttSubscribeMessage) {
        val grantedQosLevels = mutableListOf<MqttQoS>()
        subscribeMsg.topicSubscriptions().forEach { grantedQosLevels += it.qualityOfService() }
        endpoint.subscribeAcknowledge(subscribeMsg.messageId(), grantedQosLevels)
    }

    private fun handleUnsubscribe(endpoint: MqttEndpoint, unsubscribeMsg: MqttUnsubscribeMessage) {
        endpoint.unsubscribeAcknowledge(unsubscribeMsg.messageId())
    }

    /**
     * Publishes a MQTT message from the MQTT Broker to a MQTT client.
     * @param endpoint The MQTT endpoint (represents a MQTT client) to use.
     * @param topic Name of the topic for the message.
     * @param payload The data to include in the message.
     * @param qosLevel Quality of Service level to use.
     * @param duplicateMsg If true then the message can be published more than once.
     * @param retainMsg If true then the message will be retained.
     */
    fun publishMessage(
        endpoint: MqttEndpoint,
        topic: String,
        payload: String,
        qosLevel: MqttQoS = MqttQoS.AT_MOST_ONCE,
        duplicateMsg: Boolean = false,
        retainMsg: Boolean = false
    ) {
        endpoint.publish(topic, Buffer.buffer(payload), qosLevel, duplicateMsg, retainMsg)
    }
}
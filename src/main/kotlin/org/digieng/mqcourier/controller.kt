package org.digieng.mqcourier

import org.digieng.mqcourier.authorization.TopicAccessRule
import org.digieng.mqcourier.database.AccessRuleRepo
import org.digieng.mqcourier.database.Database
import org.digieng.mqcourier.database.UserRepo
import org.slf4j.LoggerFactory
import java.io.File
import kotlin.system.exitProcess

private val logger by lazy { LoggerFactory.getLogger("controller") }

internal fun setupDb() {
    val usersFile = File("${System.getProperty("user.home")}/.mqcourier/users.csv")
    val topicAccessRulesFile = File("${System.getProperty("user.home")}/.mqcourier/topic_access_rules.csv")

    logger.info("Setting up database...")
    UserRepo.removeAll()
    AccessRuleRepo.removeAll()
    checkCsvFiles(usersFile, topicAccessRulesFile)
    createUsersInDb(usersFile)
    createTopicAccessRulesInDb(topicAccessRulesFile)
    logger.info("Finished setting up database.")
}

private fun createTopicAccessRulesInDb(rulesFile: File) {
    val idPos = 0
    val allowedUsersPos = 1
    val allowedGroupsPos = 2
    val mqttActionPos = 3
    val topicPos = 4

    logger.info("Creating topic access rules...")
    rulesFile.bufferedReader().forEachLine { l ->
        if (l.startsWith(char = '"')) {
            val tmp = l.split(',').map { cleanupUserValue(it) }
            val topicAccessRule = TopicAccessRule(
                id = tmp[idPos].toInt(),
                mqttAction = fetchMqttAction(tmp[mqttActionPos]),
                topic = tmp[topicPos]
            ).apply {
                tmp[allowedUsersPos].split(";").forEach { i -> allowedUsers += i }
                tmp[allowedGroupsPos].split(";").forEach { i -> allowedGroups += i }
            }
            AccessRuleRepo.create(topicAccessRule)
        }
    }
}

private fun fetchMqttAction(value: String) = when (value.toLowerCase()) {
    MqttAction.PUBLISH.name.toLowerCase() -> MqttAction.PUBLISH
    MqttAction.SUBSCRIBE.name.toLowerCase() -> MqttAction.SUBSCRIBE
    else -> throw IllegalArgumentException("MQTT Action value must be 'Publish' or 'Subscribe'.")
}

private fun createUsersInDb(usersFile: File) {
    val usernamePos = 0
    val firstNamePos = 1
    val lastNamePos = 2
    val passwordPos = 3

    logger.info("Creating users...")
    usersFile.bufferedReader().forEachLine { l ->
        if (l.startsWith(char = '"')) {
            val tmp = l.split(',').map { cleanupUserValue(it) }
            UserRepo.create(User(
                username = tmp[usernamePos],
                firstName = tmp[firstNamePos],
                lastName = tmp[lastNamePos],
                password = tmp[passwordPos]
            ))
        }
    }
}

private fun checkCsvFiles(usersFile: File, topicAccessRuleFile: File) {
    if (!usersFile.exists()) {
        logger.error("The ${usersFile.absolutePath} file doesn't exist.")
        exitWithError()
    } else if (!topicAccessRuleFile.exists()) {
        logger.error("The ${topicAccessRuleFile.absolutePath} file doesn't exist.")
        exitWithError()
    }
}

private fun exitWithError() {
    Database.disconnect()
    exitProcess(-1)
}

internal fun topicAccessAllowed(username: String, topic: String, mqttAction: MqttAction) = AccessRuleRepo.selectAll()
    .filter { it is TopicAccessRule }
    .map { it as TopicAccessRule }
    .filter { it.mqttAction == mqttAction && it.topic == topic }
    .any { it.accessAllowed(username) }

private fun cleanupUserValue(userValue: String) = userValue.trim().replace(oldValue = "\"", newValue = "")

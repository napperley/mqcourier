package org.digieng.mqcourier.settings

import org.digieng.mqcourier.MqttBroker
import org.slf4j.LoggerFactory
import java.util.*

private val logger by lazy { LoggerFactory.getLogger("app_settings") }
internal val generalSettings by lazy { readSettings("general", "/config/app_settings.properties") }
internal val dbName by lazy { "${generalSettings["dbName"]}" }

internal fun readSettings(name: String, resPath: String): Map<String, Any> {
    val tmpMap = mutableMapOf<String, Any>()
    val properties = Properties()

    logger.info("Loading $name settings...")
    // Read the app settings from a properties file as a InputStream. When using JRE 9 and above a class from the
    // program needs to be used in order to get a resource stream that isn't null.
    properties.load(MqttBroker::class.java.getResourceAsStream(resPath))
    for ((key, value) in properties) tmpMap["$key"] = value
    return tmpMap.toMap()
}

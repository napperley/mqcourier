package org.digieng.mqcourier.settings

internal object MqttBrokerSettings {
    val port = if ("broker-port" in generalSettings) "${generalSettings["broker-port"]}".toInt() else 1883
    val host = if ("broker-host" in generalSettings) {
        "${generalSettings["broker-host"]}".replace(oldValue = "\"", newValue = "")
    } else {
        "localhost"
    }
    val sslEnabled = if ("broker-sslEnabled" in generalSettings) "${generalSettings["broker-sslEnabled"]}".toBoolean()
    else false
    val logClientDisconnect = if ("broker-logClientDisconnect" in generalSettings) {
        "${generalSettings["broker-logClientDisconnect"]}".toBoolean()
    } else {
        false
    }
    val logClientConnect = if ("broker-logClientConnect" in generalSettings) {
        "${generalSettings["broker-logClientConnect"]}".toBoolean()
    } else {
        false
    }
    val authenticationEnabled = if ("broker-authenticationEnabled" in generalSettings) {
        "${generalSettings["broker-authenticationEnabled"]}".toBoolean()
    } else {
        false
    }
    val logClientPublish = if ("broker-logClientPublish" in generalSettings) {
        "${generalSettings["broker-logClientPublish"]}".toBoolean()
    } else {
        false
    }
}
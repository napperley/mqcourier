package org.digieng.mqcourier.database

import org.digieng.mqcourier.User
import org.mapdb.Serializer

/**
 * Covers all logic for the *user* table in the database.
 * @author Nick Apperley
 */
internal object UserRepo : UserRepoBase {
    /**
     * selects a user by username.
     * @param username The name of the user to use.
     * @return A User if there is one that matches the username otherwise null.
     */
    override fun selectByUsername(username: String): User? {
        var result: User? = null
        val userAccounts = fetchUserAccounts()
        if (userAccounts != null && username in userAccounts) result = (userAccounts[username] as User)
        return result
    }

    override fun create(user: User) {
        val userAccounts = fetchUserAccounts()
        if (userAccounts != null) userAccounts[user.username] = user
        Database.fileDb?.commit()
    }

    override fun removeAll() {
        fetchUserAccounts()?.clear()
        Database.fileDb?.commit()
    }

    private fun fetchUserAccounts() = Database.fileDb?.hashMap(
        name = "user-accounts",
        keySerializer = Serializer.STRING,
        valueSerializer = Serializer.JAVA
    )?.createOrOpen()
}
package org.digieng.mqcourier.database

import org.mapdb.DB
import org.mapdb.DBMaker
import org.slf4j.LoggerFactory
import java.io.File

/**
 * Manages the database.
 * @author Nick Apperley
 */
internal object Database {
    private val logger by lazy { LoggerFactory.getLogger("Database") }
    var fileDb: DB? = null
        private set

    /**
     * Establishes a connection to the database.
     * @param dbPath Absolute path to the database.
     */
    fun connect(dbPath: String) {
        logger.info("Connecting to DB...")
        if (!File(dbPath).parentFile.exists()) File(dbPath).parentFile.mkdirs()
        if (fileDb == null) fileDb = DBMaker.fileDB(dbPath).fileMmapEnable().make()
        logger.info("Connection established.")
    }

    /** Disconnects from the database. **/
    fun disconnect() {
        fileDb?.close()
        logger.info("Disconnected DB.")
    }
}
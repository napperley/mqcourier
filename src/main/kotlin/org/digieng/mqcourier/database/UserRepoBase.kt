package org.digieng.mqcourier.database

import org.digieng.mqcourier.User

interface UserRepoBase {
    /**
     * Selects a user by username.
     * @param username The name of the user to use.
     * @return A User if there is one that matches the username otherwise null.
     */
    fun selectByUsername(username: String): User?

    fun create(user: User)
    fun removeAll()
}
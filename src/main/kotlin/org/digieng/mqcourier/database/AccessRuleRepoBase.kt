package org.digieng.mqcourier.database

import org.digieng.mqcourier.authorization.AccessRule

interface AccessRuleRepoBase {
    /** Selects all access rules. */
    fun selectAll(): Array<AccessRule>

    fun create(accessRule: AccessRule)
    fun removeAll()
}
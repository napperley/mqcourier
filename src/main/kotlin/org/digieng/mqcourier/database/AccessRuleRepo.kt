package org.digieng.mqcourier.database

import org.digieng.mqcourier.authorization.AccessRule
import org.mapdb.Serializer

object AccessRuleRepo : AccessRuleRepoBase {
    override fun selectAll(): Array<AccessRule> {
        val tmp = mutableListOf<AccessRule>()
        val accessRuleRegistry = fetchAccessRuleRegistry()
        if (accessRuleRegistry != null) tmp.addAll(accessRuleRegistry.values.map { it as AccessRule })
        return tmp.toTypedArray()
    }

    override fun create(accessRule: AccessRule) {
        val accessRuleRegistry = fetchAccessRuleRegistry()
        if (accessRuleRegistry != null) accessRuleRegistry[nextKey()] = accessRule
        Database.fileDb?.commit()
    }

    override fun removeAll() {
        fetchAccessRuleRegistry()?.clear()
        Database.fileDb?.commit()
    }

    private fun fetchAccessRuleRegistry() = Database.fileDb?.hashMap(
        name = "access-rule-registry",
        keySerializer = Serializer.INTEGER,
        valueSerializer = Serializer.JAVA
    )?.createOrOpen()

    private fun nextKey(): Int {
        var result = 0
        val accessRuleRegistry = fetchAccessRuleRegistry()
        if (accessRuleRegistry != null && accessRuleRegistry.isNotEmpty()) {
            result = (accessRuleRegistry.keys.max() ?: 0) + 1
        }
        return result
    }
}
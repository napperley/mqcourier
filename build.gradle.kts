import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "org.digieng"
version = "0.1-SNAPSHOT"

buildscript {
    val shadowVer = "2.0.1"

    repositories {
        jcenter()
        mavenCentral()
    }

    dependencies {
        classpath("com.github.jengelman.gradle.plugins:shadow:$shadowVer")
    }
}

apply {
    plugin("com.github.johnrengelman.shadow")
}

plugins {
    kotlin(module = "jvm") version "1.2.51"
    application
}

application {
    mainClassName = "org.digieng.mqcourier.MainKt"
}

val compileKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions.jvmTarget = "1.8"
}
val shadowJar by tasks.getting(ShadowJar::class) {
    baseName = "mq_courier"
    version = "0.1-SNAPSHOT"
    classifier = ""
    manifest {
        attributes["Main-Class"] = application.mainClassName
    }
    append("src/main/resources")
}

repositories {
    mavenCentral()
}

dependencies {
    val kotlinVer = "1.2.51"
    val vertxVer = "3.5.2"
    val log4jVer = "2.0"
    val slf4jVer = "1.7.7"
    val mapDbVer = "3.0.7"

    compile(kotlin(module = "stdlib-jdk8", version = kotlinVer))
    compile("io.vertx:vertx-mqtt:$vertxVer")
    compile("org.apache.logging.log4j:log4j-core:$log4jVer")
    compile("org.slf4j:slf4j-api:$slf4jVer")
    compile("org.slf4j:slf4j-log4j12:$slf4jVer")
    compile("org.mapdb:mapdb:$mapDbVer")
}